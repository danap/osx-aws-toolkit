//
//  aws_toolkitTests.m
//  aws-toolkitTests
//
//  Created by Dana P'Simer on 9/29/11.
//  Copyright 2011 DHP Technologies, Inc. All rights reserved.
//

#import "aws_toolkitTests.h"
#import "AmazonClientManager.h"
#import "AmazonS3Client.h"
#import "AmazonLogger.h"
#import "AmazonServiceException.h"

static NSString* TEST_BUCKET_NAME = @"com.bluesoftdev.aws-toolkit-test-bucket";

@implementation aws_toolkitTests

- (void)setUp
{
  [super setUp];

  [AmazonLogger verboseLogging];
  [AmazonLogger consoleLogger];
  [AmazonClientManager validateCredentials];
}

- (void)tearDown
{
  [super tearDown];
}

- (void)testListBuckets
{

  AmazonS3Client* s3 = [AmazonClientManager s3];
  NSEnumerator* e = [[s3 listBuckets] objectEnumerator];
  NSLog(@"List of S3 Buckets:");
  S3Bucket* bucket = nil;
  while(bucket = [e nextObject]) {
    NSLog(@"bucket: %@/%@",bucket.name,bucket.creationDate);
  }
}

- (void)testCreateBucket
{
  
  AmazonS3Client* s3 = [AmazonClientManager s3];
  S3ListBucketsRequest* listRequest = [[[S3ListBucketsRequest alloc] init] autorelease];
  
  listRequest.bucket = TEST_BUCKET_NAME;
  
  NSLog(@"asking if s3 has a bucket named %@",TEST_BUCKET_NAME);
  @try {
    S3ListBucketsResponse* listResponse = [s3 listBuckets: listRequest];
    STAssertNotNil( listResponse, @"listResponse should not be null");
    if ( listResponse.listBucketsResult != nil && listResponse.listBucketsResult.buckets != nil) {
      if ( [listResponse.listBucketsResult.buckets count] > 0 ) {
        NSLog(@"%@ was find, deleting it",TEST_BUCKET_NAME);
        [s3 deleteBucket: [[[S3DeleteBucketRequest alloc] initWithName: TEST_BUCKET_NAME] autorelease]];
      }
    }
  } @catch(AmazonServiceException* e) {
    STAssertEqualObjects(e.errorCode, @"NoSuchBucket", @"Unexpected Exception %@", e); 
  }
  NSLog(@"Creating %@",TEST_BUCKET_NAME);
  S3CreateBucketRequest* request = [[[S3CreateBucketRequest alloc] initWithName: TEST_BUCKET_NAME] autorelease];
  S3CreateBucketResponse* response = [s3 createBucket: request];
  STAssertNotNil(response, @"response should not be nil");
  NSLog(@"%@ Created",TEST_BUCKET_NAME);
}

@end
